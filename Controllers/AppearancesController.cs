﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Appearance;
using AtomicCore.Model.Calendar;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Appearance = AtomicData.Appearance;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class AppearancesController : ApiController
    {
        [HttpGet]
        [GET("appearances")]
        public List<Appearance> GetAppearances()
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                return AppearanceObject.GetAppearances(clientid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("appearances/{id}/status")]
        public Status GetAppearanceStatus(int id)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                return AppearanceObject.GetAppearanceStatus(id, clientid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [GET("appearances/{id}/approve")]
        public AppearanceMeta ApproveAppearance(int id)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                return AppearanceObject.Approve(id, clientid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [GET("appearances/{id}/cancel")]
        public AppearanceMeta CancelAppearance(int id)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                return AppearanceObject.Cancel(id, clientid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("appearances/{id}")]
        public AppearanceMeta GetAppearanceById(int id)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                return AppearanceObject.GetAppearance(id, clientid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("appearances/{id}")]
        public AppearanceMeta UpdateAppearance([FromBody]AppearancePayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    if (payload.AppearanceId == 0)
                        throw new Exception("Appearance must have an Appearance ID set.");

                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var current = AppearanceObject.Get(payload.AppearanceId, clientid);
                    var currentCalendar = CalendarObject.GetCalendarEntry(current.CalendarId, clientid);
                    

                    if(payload.SetStart > 0)
                    {
                        var start = Helper.FromUnixTime(payload.SetStart);
                        currentCalendar.Starting = start;
                    }

                    if(payload.SetEnd > 0)
                    {
                        var end = Helper.FromUnixTime(payload.SetEnd);
                        currentCalendar.Ending = end;
                    }
                    
                    var duration = (int)(currentCalendar.Starting - currentCalendar.Ending).TotalHours;
                    
                    if(payload.AgreementId > 0)
                    {
                        current.AgreementId = payload.AgreementId;
                    }

                    if (payload.PurchaserId > 0)
                    {
                        current.PurchaserId = payload.PurchaserId;
                    }

                    if (payload.PlaceId > 0)
                    {
                        current.PlaceId = payload.PlaceId;
                    }

                    if (payload.ContractingCompany > 0)
                    {
                        current.ContractingCompany = payload.ContractingCompany;
                    }

                    if (payload.OperatingCurrency > 0)
                    {
                        current.OperatingCurrency = payload.OperatingCurrency;
                    }

                    if(payload.RiderId > 0)
                    {
                        current.RiderId = payload.RiderId;
                    }

                    if(payload.Description != null)
                    {
                        current.Title = payload.Description;
                    }

                    if (payload.ItineraryNotes != null)
                    {
                        current.Notes = payload.ItineraryNotes;
                    }
                    
                    if (payload.CashToCollect != null && payload.CashToCollect.Amount > 0)
                    {
                        current.CashToCollect = payload.CashToCollect.Amount;
                        current.CashToCollectCurrency = payload.CashToCollect.CurrencyId;
                    }

                    var result = AppearanceObject.UpdateAppearance(current, currentCalendar.Starting, payload.Contacts.ToList(), duration);
                    return AppearanceObject.MarshallAppearance(result);
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("appearances")]
        public AppearanceMeta CreateApperance([FromBody]AppearancePayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    if (payload.SetStart == 0)
                        throw new Exception("Appearance must have a set starting time set.");

                    if (payload.SetEnd == 0)
                        throw new Exception("Appearance must have a set ending time set.");

                    if (payload.PurchaserId == 0)
                        throw new Exception("PurchaserId must be set.");

                    if (payload.RiderId == 0)
                        throw new Exception("RiderId must be set.");

                    if (payload.PlaceId == 0)
                        throw new Exception("PlaceId must be set.");

                    if (payload.TalentId == 0)
                        throw new Exception("TalentId must be set.");

                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var start = Helper.FromUnixTime(payload.SetStart);
                    var end = Helper.FromUnixTime(payload.SetEnd);
                    var duration = (int) (start - end).TotalHours;

                    var appearance = new Appearance
                        {
                            PurchaserId = payload.PurchaserId,
                            RiderId = payload.RiderId,
                            ClientId = clientid,
                            FeeModel = payload.FeeModel,
                            ContractingCompany = payload.ContractingCompany,
                            OperatingCurrency = payload.OperatingCurrency,
                            AgreementId = payload.AgreementId,
                            Notes = payload.ItineraryNotes,
                            PlaceId = payload.PlaceId,
                            TalentId = payload.TalentId,
                            Status = 1
                        };

                    if (payload.CashToCollect != null && payload.CashToCollect.Amount > 0)
                    {
                        appearance.CashToCollect = payload.CashToCollect.Amount;
                        appearance.CashToCollectCurrency = payload.CashToCollect.CurrencyId;
                    }

                    var result = AppearanceObject.CreateAppearance(appearance, start, payload.Contacts.ToList(), duration);
                    return AppearanceObject.MarshallAppearance(result);
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}