﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;

namespace Uranium.Controllers
{
    public class PlanController : ApiController
    {
        [HttpGet]
        [GET("plans")]
        public IEnumerable<Plan> Get()
        {
            try
            {
                return PlanObject.GetPlans();
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("plans/{id:int}")]
        public Plan GetPlanById(int id)
        {
            try
            {
                return PlanObject.GetPlan(id);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}