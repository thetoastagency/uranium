﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtomicCore;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using AtomicData;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class ContractingCompaniesController : ApiController
    {
        [HttpGet]
        [GET("contracting-companies")]
        public IEnumerable<ContractingCompany> GetCompanies()
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                return ContractingCompanyObject.GetContractingCompanies(clientid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpDelete]
        [DELETE("contracting-companies/{company}")]
        public HttpResponseMessage DeleteContractingCompany(int company)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                var c = ContractingCompanyObject.GetContractingCompany(company, clientid);
                if(c != null)
                {
                    c.Active = false;
                    ContractingCompanyObject.UpdateContractingCompany(c);
                }
                
                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"message\":\"Success.}]")
                };
                return happy;

            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpPost]
        [POST("contracting-companies/{company}")]
        public HttpResponseMessage UpdateContractingCompany([FromBody]ContractingCompanyPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];

                    var c = new ContractingCompany
                    {
                        CompanyName = payload.CompanyName,
                        AddressId = payload.AddressId,
                        Active = payload.Active,
                        ClientId = clientid,
                        ContractingCompanyId = payload.ContractingCompanyId,
                        OperatingCurrencyId = payload.OperatingCurrency
                    };

                    var created = ContractingCompanyObject.UpdateContractingCompany(c);

                    if (!Helpers.Generics.IsNullOrDefault(payload.Details))
                    {
                        foreach (var f in payload.Details.Select(detail => new FinancialDetail
                            {
                                ClientId = clientid, ContractingCompany = created.ContractingCompanyId, FinancialDetailId = detail.Id, FinancialDetailItem = detail.Detail, FinancialDetailType = detail.Type
                            }))
                        {
                            FinancialDetailObject.UpdateFinancialDetail(f);
                        }
                    }

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"ContractingCompany\": " + JsonConvert.SerializeObject(created, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    
        [HttpPost]
        [POST("contracting-companies")]
        public HttpResponseMessage CreateContractingCompany([FromBody]ContractingCompanyPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];

                    var c = new ContractingCompany
                    {
                        CompanyName = payload.CompanyName,
                        AddressId = payload.AddressId,
                        ClientId = clientid,
                        ContractingCompanyId = payload.ContractingCompanyId,
                        OperatingCurrencyId = payload.OperatingCurrency
                    };

                    var created = ContractingCompanyObject.CreateContractingCompany(c);

                    if (!Helpers.Generics.IsNullOrDefault(payload.Details))
                    {
                        foreach (var f in payload.Details.Select(detail => new FinancialDetail
                            {
                                ClientId = clientid, ContractingCompany = created.ContractingCompanyId, FinancialDetailId = detail.Id, FinancialDetailItem = detail.Detail, FinancialDetailType = detail.Type
                            }))
                        {
                            FinancialDetailObject.CreateFinancialDetail(f);
                        }
                    }

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"ContractingCompany\": " + JsonConvert.SerializeObject(created, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}