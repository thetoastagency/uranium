﻿using System;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Billing;
using AttributeRouting.Web.Http;
using Uranium.Authorize;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class BillingController : ApiController
    {
        [HttpGet]
        [GET("billing/account")]
        public BillingAccount Get()
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return BillingObject.GetBillingAccount(client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }
    }
}