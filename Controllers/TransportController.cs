﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Calendar;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class TransportController : ApiController
    {
        [HttpGet]
        [GET("transport")]
        public IEnumerable<TransportMeta> Get()
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return TransportObject.GetTransports(client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }


        [HttpGet]
        [NullObjectActionFilter]
        [GET("transport/{id}")]
        public TransportMeta GetTransportById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return TransportObject.GetTransport(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("transport")]
        public HttpResponseMessage CreateTranport([FromBody]TransportPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                if(payload.Pickup == null)
                    throw new Exception("Pickup object must be defined.");

                if (payload.Dropoff == null)
                    throw new Exception("Dropoff object must be defined."); 

                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var pickup = Helper.FromUnixTime(payload.Pickup.DateTime);
                    var dropoff = Helper.FromUnixTime(payload.Dropoff.DateTime);

                    var t = new Taxi
                    {
                        Passenger = payload.PassengerContactId,
                        Carrier = payload.Carrier,
                        ClientId = clientid,
                        Cost = payload.Cost,
                        Currency = payload.CurrencyId,
                        FreeDropOff = payload.Dropoff.Place,
                        PlaceIdDropOff = payload.Dropoff.PlaceId,
                        FreePickupFrom = payload.Pickup.Place,
                        PlaceIdPickupFrom = payload.Pickup.PlaceId,
                        Notes = payload.Notes
                    };

                    var updated = TransportObject.CreateTransport(t, pickup, dropoff, payload.TalentId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.TaxiId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("transport/{id}")]
        public HttpResponseMessage UpdateTransport([FromBody]TransportPayload payload, int id)
        {
            if (ModelState.IsValid && payload != null)
            {
                if (payload.Pickup == null)
                    throw new Exception("Pickup object must be defined.");

                if (payload.Dropoff == null)
                    throw new Exception("Dropoff object must be defined.");

                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var pickup = Helper.FromUnixTime(payload.Pickup.DateTime);
                    var dropoff = Helper.FromUnixTime(payload.Dropoff.DateTime);

                    var t = new Taxi
                    {
                        TaxiId = id,
                        Passenger = payload.PassengerContactId,
                        Carrier = payload.Carrier,
                        ClientId = clientid,
                        Cost = payload.Cost,
                        Currency = payload.CurrencyId,
                        FreeDropOff = payload.Dropoff.Place,
                        PlaceIdDropOff = payload.Dropoff.PlaceId,
                        FreePickupFrom = payload.Pickup.Place,
                        PlaceIdPickupFrom = payload.Pickup.PlaceId,
                        PayeeId = payload.PayeeId,
                        Notes = payload.Notes
                    };

                    var updated = TransportObject.UpdateTransport(t, pickup, dropoff, payload.TalentId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.TaxiId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

    }
}