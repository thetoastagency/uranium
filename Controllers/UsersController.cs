﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.User;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class UsersController : ApiController
    {
        [HttpGet]
        [NullObjectActionFilter]
        [GET("users/{id:int}")]
        public User GetUserById(int id)
        {
            try
            {
                var guid = ControllerContext.RouteData.Values["user-guid"].ToString();
                return UserObject.GetUser(id, guid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("users")]
        public List<User> GetUsers()
        {
            try
            {
                var guid = ControllerContext.RouteData.Values["user-guid"].ToString();
                return UserObject.GetUsers(guid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("users")]
        public HttpResponseMessage CreateUser([FromBody]UserPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var client = ClientObject.GetClient(clientid);

                    var host = Request.Headers.Host;
                    var agent = Request.Headers.UserAgent.ToString();

                    var user = new User
                        {
                            IsMasterAccount = false,
                            UserLogin = payload.Email,
                            LastBrowserAgent = agent,
                            LastIPAddress = host
                        };
                    var created = UserObject.CreateUser(client, user, HttpUtility.UrlDecode(payload.Name),
                                                        HttpUtility.UrlDecode(payload.Password));
                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(created, Formatting.None))
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpGet]
        [GET("profile")]
        public DetailedUser GetProfile()
        {
            try
            {
                var id = (int)ControllerContext.RouteData.Values["user-id"];
                return UserObject.GetSelf(id);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}