﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class AgreementsController : ApiController
    {
        [HttpGet]
        [GET("agreements")]
        public IEnumerable<Agreement> Get()
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return (AgreementObject.GetAgreements(client)).Select(x => x);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("agreements/{id}")]
        public Agreement GetAgreementById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return AgreementObject.GetAgreement(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("agreements")]
        public HttpResponseMessage CreateAgreement([FromBody]AgreementPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var guid = ControllerContext.RouteData.Values["user-guid"].ToString();
                    var user = UserObject.GetUser(guid);

                    if (!ClientObject.HasClientAccessToAgreement(clientid, payload.AgreementId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var agreement = new Agreement
                    {
                        AgreementId = payload.AgreementId,
                        Content = payload.Agreement,
                        ClientId = clientid,
                        Accommodation = payload.Accommodation,
                        AgreementReference = payload.Reference,
                        AppearanceId = payload.AppearanceId,
                        DrawnBy = user.UserId,
                        PaymentTerms = payload.PaymentTerms,
                        Personal = payload.Personal,
                        Ground = payload.Ground,
                        Transport = payload.Transport,
                        TechnicalDiagram = payload.TechnicalDiagramUrl,
                        Technical = payload.Technical,
                        Misc = payload.Misc,
                        Marketing = payload.Marketing,
                    };

                    var created = AgreementObject.Create(agreement, user.UserId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(created.AgreementId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("agreements/{id}")]
        public HttpResponseMessage UpdateAgreement([FromBody]AgreementPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var guid = ControllerContext.RouteData.Values["user-guid"].ToString();
                    var user = UserObject.GetUser(guid);

                    if (!ClientObject.HasClientAccessToAgreement(clientid, payload.AgreementId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var agreement = new Agreement
                    {
                        AgreementId = payload.AgreementId,
                        Content = payload.Agreement,
                        ClientId = clientid,
                        Accommodation = payload.Accommodation,
                        AgreementReference = payload.Reference,
                        AppearanceId = payload.AppearanceId,
                        DrawnBy = user.UserId,
                        PaymentTerms = payload.PaymentTerms,
                        Personal = payload.Personal,
                        Ground = payload.Ground,
                        Transport = payload.Transport,
                        TechnicalDiagram = payload.TechnicalDiagramUrl,
                        Technical = payload.Technical,
                        Misc = payload.Misc,
                        Marketing = payload.Marketing,
                    };

                    var updated = AgreementObject.Update(agreement, user.UserId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.AgreementId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}