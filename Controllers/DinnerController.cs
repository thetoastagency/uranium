﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Calendar;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class DinnerController : ApiController
    {
        [HttpGet]
        [GET("dinner")]
        public IEnumerable<DinnerMeta> Get()
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return DinnerObject.GetDinners(client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("dinner/{id}")]
        public DinnerMeta GetDinnerById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return DinnerObject.GetDinner(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("dinner")]
        public HttpResponseMessage CreateDinner([FromBody]DinnerPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                if (payload.DinerId == 0)
                    throw new Exception("Diner Id can not be zero.");

                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var starting = Helper.FromUnixTime(payload.Starting);
                    var ending = Helper.FromUnixTime(payload.Ending);

                    var d = new Dinner
                    {
                        AttachedAppearance = payload.AttachedAppearance,
                        ClientId = clientid,
                        Cost = payload.Cost,
                        Description = payload.Description,
                        DinerId = payload.DinerId,
                        PlaceId = payload.PlaceId,
                        PayeeId = payload.PayeeId
                    };

                    var updated = DinnerObject.CreateDinner(d, starting, ending);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.DinnerId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("dinner/{id}")]
        public HttpResponseMessage UpdateDinner([FromBody]DinnerPayload payload, int id)
        {
            if (ModelState.IsValid && payload != null)
            {
                if (payload.DinerId == 0)
                    throw new Exception("Diner Id can not be zero.");

                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var starting = Helper.FromUnixTime(payload.Starting);
                    var ending = Helper.FromUnixTime(payload.Ending);

                    var d = new Dinner
                    {
                        DinnerId = id,
                        AttachedAppearance = payload.AttachedAppearance,
                        ClientId = clientid,
                        Cost = payload.Cost,
                        Description = payload.Description,
                        DinerId = payload.DinerId,
                        PlaceId = payload.PlaceId,
                        PayeeId = payload.PayeeId
                    };

                    var updated = DinnerObject.UpdateDinner(d, starting, ending);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.DinnerId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}