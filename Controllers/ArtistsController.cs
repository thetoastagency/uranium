﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Talent;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;
using Uranium.Filters;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class ArtistsController : ApiController
    {
        [HttpGet]
        [GET("artists")]
        public IEnumerable<Artist> Get()
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                var client = UserObject.GetUser(token).ClientId;
                return ArtistObject.GetArtists(client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("artists/{id}")]
        public Artist GetArtistById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return ArtistObject.GetArtist(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}