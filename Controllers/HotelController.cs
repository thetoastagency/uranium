﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class HotelController : ApiController
    {
        [HttpGet]
        [GET("hotelstays")]
        public IEnumerable<HotelStay> Get()
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return HotelObject.GetHotelStays(client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("hotelstays/{id}")]
        public HotelStay GetHotelStayById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return HotelObject.GetHotelStay(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("hotelstays/{id}")]
        public HttpResponseMessage UpdateHotel([FromBody]HotelsPayload payload, int id)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var checkin = Helper.FromUnixTime(payload.CheckinDateTime);
                    var checkout = Helper.FromUnixTime(payload.CheckoutDateTime);

                    var hs = new HotelStay
                    {
                        ClientId = clientid,
                        HotelStayId = id,
                        PlaceId = payload.PlaceId,
                        Cost = payload.Cost,
                        Currency = payload.CurrencyId,
                        Guest = payload.GuestContactId,
                        HotelClass = payload.HotelClass,
                        PayeeId = payload.PayeeId,
                        ReservationNumber = payload.ReservationNumber,
                        StayDescription = payload.StayDescription
                    };

                    var updated = HotelObject.UpdateHotelStay(hs, checkin, checkout, payload.TalentId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.HotelStayId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("hotelstays")]
        public HttpResponseMessage CreateHotel([FromBody]HotelsPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var checkin = Helper.FromUnixTime(payload.CheckinDateTime);
                    var checkout = Helper.FromUnixTime(payload.CheckoutDateTime);

                    var hs = new HotelStay
                    {
                        ClientId = clientid,
                        PlaceId = payload.PlaceId,
                        Cost = payload.Cost,
                        Currency = payload.CurrencyId,
                        Guest = payload.GuestContactId,
                        HotelClass = payload.HotelClass,
                        PayeeId = payload.PayeeId,
                        ReservationNumber = payload.ReservationNumber,
                        StayDescription = payload.StayDescription
                    };

                    var created = HotelObject.CreateHotelStay(hs, checkin, checkout, payload.TalentId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"HotelStay\": " + JsonConvert.SerializeObject(created, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}