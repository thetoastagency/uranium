﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.City;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    public class CitiesController : ApiController
    {
        [HttpGet]
        [GET("cities")]
        public IEnumerable<City> Get([FromUri]int from = 0, [FromUri]int wanted = 50)
        {
            try
            {
                return (CityObject.GetCitiesPaged(from, wanted)).Select(x => x);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("cities/search")]
        public IEnumerable<CityMeta> Search([FromUri]string query, [FromUri]int limit = 50)
        {
            try
            {
                return CityObject.Search(query, limit);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("cities/{id:int}")]
        public City GetCityById(int id)
        {
            try
            {
                return CityObject.GetCity(id);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}