﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Models;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    public class AuthenticationController : ApiController
    {
        [HttpPost]
        [POST("login")]
        public HttpResponseMessage Login([FromBody]AuthenticationPayload payload)
        {
            if(ModelState.IsValid && payload != null)
            {
                var host = Request.Headers.Host;
                var agent = Request.Headers.UserAgent.ToString();

                var authenticateduser = UserObject.Authenticate(HttpUtility.UrlDecode(payload.Email),
                                                        HttpUtility.UrlDecode(payload.Password),
                                                        HttpUtility.UrlDecode(agent), HttpUtility.UrlDecode(host));


                var resp = new HttpResponseMessage(authenticateduser.ResponseCode)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(authenticateduser, Formatting.None))
                };
                return resp;

            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.Forbidden)
                {
                    Content = new StringContent("[{\"message\":\"Payload invalid or missing. Unauthenticated.}]")
                };
                throw new HttpResponseException(resp);
            }
        }
    }
}