﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class RidersController : ApiController
    {
        [HttpGet]
        [GET("riders/{talent}")]
        public List<Rider> Get(int talent)
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                var client = UserObject.GetUser(token).ClientId;
                return RiderObject.GetRiders(talent, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("riders/{talent}/{id}")]
        public Rider Get(int talent, int rider)
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                var client = UserObject.GetUser(token).ClientId;
                return RiderObject.GetRider(rider, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("riders")]
        public HttpResponseMessage CreateRider([FromBody]RiderPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    
                    if (!ClientObject.HasClientAccessToTalent(clientid, payload.TalentId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var rider = new Rider
                    {
                        RiderId = payload.RiderId,
                        ArtistId = payload.ArtistId,
                        TalentId = payload.TalentId,
                        Personal = payload.Personal,
                        Ground = payload.Ground,
                        Transport = payload.Transport,
                        Accommodation = payload.Accommodation,
                        Technical = payload.Technical,
                        TechnicalDiagram = payload.TechnicalDiagram,
                        Misc = payload.Misc,
                        Marketing = payload.Marketing,
                        PreferredAirport = payload.PreferredAirport,
                        PreferredAirline = payload.PreferredAirline,
                        PreferredHotel = payload.PreferredHotel
                    };

                    var created = RiderObject.CreateRider(rider);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(created.RiderId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("riders/{id}")]
        public HttpResponseMessage UpdateRider([FromBody]RiderPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    
                    if (!ClientObject.HasClientAccessToTalent(clientid, payload.TalentId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var rider = new Rider
                    {
                        RiderId = payload.RiderId,
                        ArtistId = payload.ArtistId,
                        TalentId = payload.TalentId,
                        Personal = payload.Personal,
                        Ground = payload.Ground,
                        Transport = payload.Transport,
                        Accommodation = payload.Accommodation,
                        Technical = payload.Technical,
                        TechnicalDiagram = payload.TechnicalDiagram,
                        Misc = payload.Misc,
                        Marketing = payload.Marketing,
                        PreferredAirport = payload.PreferredAirport,
                        PreferredAirline = payload.PreferredAirline,
                        PreferredHotel = payload.PreferredHotel
                    };

                    var updated = RiderObject.UpdateRider(rider);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.RiderId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

    }
}