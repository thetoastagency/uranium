﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    public class TimezoneController : ApiController
    {
        [HttpGet]
        [GET("timezones")]
        public IEnumerable<Timezone> Get()
        {
            try
            {
                return TimezoneObject.GetTimezones();
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("timezones/{id}")]
        public Timezone GetTimezoneById(int timezoneid)
        {
            try
            {
                return TimezoneObject.GetTimezone(timezoneid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}