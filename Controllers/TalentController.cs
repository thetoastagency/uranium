﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Talent;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class TalentController : ApiController
    {
        [HttpGet]
        [GET("talents")]
        public IEnumerable<Talent> Get(int from = 0, int wanted = 50)
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                var client = UserObject.GetUser(token).ClientId;
                return (TalentObject.GetTalentsPaged(client, from, wanted)).Select(x => x);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("talents/{id}")]
        public TalentMeta GetTalentById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return TalentObject.GetTalentExtended(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("talents/{id}")]
        public HttpResponseMessage UpdateTalent([FromBody]TalentsPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var client = ClientObject.GetClient(clientid);

                    var talent = new Talent
                    {
                        TalentId = payload.TalentId,
                        TalentName = payload.TalentName,
                        AgencyCut = payload.AgencyCut,
                        Avatar = payload.Avatar,
                        Bio = payload.Bio,
                        ClientId = client.ClientId,
                        DefactoFee = payload.DefactoFee,
                        InvoicePrefix = payload.InvoicePrefix,
                        PreferredCurrency = payload.PreferredCurrencyId,
                        Manager = payload.Manager
                    };

                    var updated = TalentObject.UpdateTalent(talent);

                    foreach (var artist in payload.Artists)
                    {
                        TalentObject.AddArtistToTalent(artist, updated.TalentId);
                    }

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.TalentId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("talents")]
        public HttpResponseMessage CreateTalent([FromBody]TalentsPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var client = ClientObject.GetClient(clientid);

                    var talent = new Talent
                    {
                        TalentName = payload.TalentName,
                        AgencyCut = payload.AgencyCut,
                        Avatar = payload.Avatar,
                        Bio = payload.Bio,
                        ClientId = client.ClientId,
                        DefactoFee = payload.DefactoFee,
                        InvoicePrefix = payload.InvoicePrefix,
                        PreferredCurrency = payload.PreferredCurrencyId,
                        Manager = payload.Manager
                    };
                    
                    var created = TalentObject.CreateTalent(talent);
                    
                    foreach(var artist in payload.Artists)
                    {
                        TalentObject.AddArtistToTalent(artist, created);
                    }
                    
                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(created, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}