﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Contact;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class ContactsController : ApiController
    {
        [HttpGet]
        [GET("contacts")]
        public IEnumerable<LiteContact> Get(int from = 0, int wanted = 1000000, [FromUri]int filter = 0)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return filter == 0 ? ContactObject.GetLiteContacts(client) : ContactObject.GetLiteContacts(client, filter);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
            
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("contacts/{id:int}")]
        public DetailedContact GetContactById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return ContactObject.GetFullFatClient(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("contact-types")]
        public List<ContactTypeEnum> GetContactTypes()
        {
            try
            {
                return ContactObject.GetContactTypes();
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("contact-detail-types")]
        public List<ContactDetailTypeEnum> GetContactDetailTypes()
        {
            try
            {
                return ContactObject.GetContactDetailTypes();
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("contacts/{id:int}")]
        public HttpResponseMessage UpdateContact([FromBody]ContactPayload payload, int id)
        {
            if (ModelState.IsValid && payload != null)
            {
                if(payload.ContactId != id)
                {
                    var bad = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("[{\"message\":\"Payload ID and passed ID do not match.}]")
                    };
                    throw new HttpResponseException(bad);
                }
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var contactid = id;
                    var type = 9;

                    if (!Helpers.Generics.IsNullOrDefault(payload.Type)) { type = payload.Type; }

                    var contact = new Contact
                    {
                        ContactId = id,
                        ClientId = clientid,
                        Name = payload.Name,
                        Type = type,
                        Avatar = payload.Avatar,
                        AccessEnabled = payload.AccessEnabled
                    };

                    ContactObject.UpdateContact(contact);

                    if(type == 8)
                    {
                        if(string.IsNullOrEmpty(payload.Artist.DateOfBirth))
                        {
                            payload.Artist.DateOfBirth = DateTime.UtcNow.ToString();
                        }

                        var artist = new Artist
                        {
                            Fullname = payload.Name,
                            AirlinePreference = payload.Artist.AirlinePreference,
                            AirportPreference = payload.Artist.AirportPreference,
                            ClientId = clientid,
                            ContactId = contactid,
                            AllowAccess = payload.Artist.AllowAccess,
                            Avatar = payload.Artist.Avatar,
                            DateOfBirth = Convert.ToDateTime(payload.Artist.DateOfBirth),
                            Manager = payload.Artist.Manager,
                            OperatingCurrency = payload.Artist.OperatingCurrency
                        };

                        ArtistObject.UpdateArtist(artist);
                    }
                    
                    if(!Helpers.Generics.IsNullOrDefault(payload.Addresses))
                    {
                        var addresses = payload.Addresses.Select(i => new AtomicData.Address
                            {
                                ClientId = clientid,
                                AddressId = i.AddressId,
                                AddressLine1 = i.Address1,
                                AddressLine2 = i.Address2,
                                AddressLine3 = i.Address3,
                                AddressLine4 = i.Address4,
                                Postcode = i.Postcode,
                                CityId = i.CityId,
                                CountryId = i.CountryId
                            }).ToList();
                        AddressObject.UpdateAddresses(addresses, contact.ContactId);
                    }

                    if (!Helpers.Generics.IsNullOrDefault(payload.ContactDetail))
                    {
                        var tuple = payload.ContactDetail.Select(i => new Tuple<int, int, string>(contactid, i.Type, i.Detail)).ToList();
                        ContactObject.UpdateContactDetail(tuple, contactid, clientid);
                    }

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Contact updated successfully.}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("contact")]
        public HttpResponseMessage CreateContact([FromBody]ContactPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var type = 9;
                    
                    if(!Helpers.Generics.IsNullOrDefault(payload.Type)) { type = payload.Type; }
                    
                    var contact = new Contact
                    {
                        Name = payload.Name,
                        Type = type,
                        ClientId = clientid
                    };
                    var created = ContactObject.CreateContact(contact);
                    var contactid = created.ContactId;

                    if(type == 8)
                    {
                        if (string.IsNullOrEmpty(payload.Artist.DateOfBirth))
                        {
                            payload.Artist.DateOfBirth = DateTime.UtcNow.ToString();
                        }
                        var artist = new Artist
                        {
                            Fullname = payload.Name,
                            AirlinePreference = payload.Artist.AirlinePreference,
                            AirportPreference = payload.Artist.AirportPreference,
                            ClientId = clientid,
                            ContactId = contactid,
                            AllowAccess = payload.Artist.AllowAccess,
                            Avatar = payload.Artist.Avatar,
                            DateOfBirth = Convert.ToDateTime(payload.Artist.DateOfBirth),
                            Manager = payload.Artist.Manager,
                            OperatingCurrency = payload.Artist.OperatingCurrency
                        };

                        ArtistObject.CreateArtist(artist);
                    }


                    if (!Helpers.Generics.IsNullOrDefault(payload.ContactDetail))
                    {
                        var tuple = payload.ContactDetail.Select(i => new Tuple<int, int, string>(contactid, i.Type, i.Detail)).ToList();
                        ContactObject.AddContactDetail(tuple, contactid, clientid);
                    }
                    
                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(created, Formatting.None))
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}