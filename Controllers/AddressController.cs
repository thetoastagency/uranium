﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Contact;
using AttributeRouting.Web.Http;
using Uranium.Authorize;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class AddressController : ApiController
    {
        [HttpGet]
        [GET("contacts/address/{id:int}")]
        public IEnumerable<Address> Get(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return ContactObject.GetAddresses(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpGet]
        [GET("address/{id:int}")]
        public AtomicData.Address GetAddress(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return AddressObject.GetAddress(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }
    }
}