﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    public class CurrencyController : ApiController
    {
        [HttpGet]
        [GET("currencies")]
        public IEnumerable<AtomicData.Currency> Get()
        {
            try
            {
                return CurrencyObject.GetCurrencies();
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("currencies/{code}")]
        public AtomicData.Currency GetCurrencyByCode(string code)
        {
            try
            {
                return CurrencyObject.GetCurrency(code);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}