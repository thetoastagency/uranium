﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class AdvisoriesController : ApiController
    {
        [HttpGet]
        [GET("advisories/{id}")]
        public IEnumerable<Advisory> Get(int id)
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                return (AdvisoryObject.GetUnreadAdvisories(token)).Select(x => x);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("advisories/advisory/{id}")]
        public Advisory GetById(int id)
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                return AdvisoryObject.GetAdvisory(id, token);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

            
        }
    }
}