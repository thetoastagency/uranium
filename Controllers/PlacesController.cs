﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Geolocation;
using AtomicCore.Model.Place;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class PlacesController : ApiController
    {
        [HttpGet]
        [GET("places/search/{what}/{city:int}")]
        public IEnumerable<Place> Search(string what, int city)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return PlacesObject.Search(HttpUtility.UrlDecode(what), city, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpGet]
        [GET("places/{id:int}")]
        public Place GetPlaceById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return PlacesObject.GetPlace(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("places")]
        public List<PlaceWithAddress> GetPlaces()
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return PlacesObject.GetPlaces(client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("places/{foursquare}")]
        public HttpResponseMessage CreatePlace([FromUri]string foursquare)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                var created = PlacesObject.GenerateFromFoursquare(foursquare, clientid);

                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"message\":\"Success.\", \"Place\": " + JsonConvert.SerializeObject(created, Formatting.None) + "}]")
                };
                return happy;
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("places")]
        public HttpResponseMessage CreatePlace([FromBody]PlacesPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var type = 1;

                    if (!Helpers.Generics.IsNullOrDefault(payload.Type)) { type = payload.Type; }

                    var p = new Place
                        {
                            PlaceName = payload.Name,
                            ClientId = clientid,
                            Description = payload.Description,
                            Foursquare = payload.Foursquare,
                            Lat = payload.Lat,
                            Lon = payload.Lon,
                            PlaceTypeId = type
                        };

                    if (!Helpers.Generics.IsNullOrDefault(payload.Address))
                    {
                        if(Helpers.Generics.IsNullOrDefault(payload.Lat) || Helpers.Generics.IsNullOrDefault(payload.Lon))
                            throw new Exception("When providing an address, a valid lat/lon must be passed to geolocate.");

                        var ll = new LatLon {Lat = payload.Lat, Lon = payload.Lon};
                        var clm = Geolocation.Geolocate(ll);
                        
                        if(clm.CityId == 0)
                            throw new Exception("Cannot geolocate city, perhaps city does not exist?");

                        var placetype = PlacesObject.GetPlaceType(type);

                        var a = new Address
                        {
                            AddressLine1 = payload.Address.Address1,
                            AddressLine2 = payload.Address.Address2,
                            AddressLine3 = payload.Address.Address3,
                            AddressLine4 = payload.Address.Address4,
                            Postcode = payload.Address.Postcode,
                            AddressTag = placetype.Description.ToLower(),
                            CityId = clm.CityId ?? 0,
                            CountryId = clm.CountryId ?? 0,
                            Lat = ll.Lat,
                            Lon = ll.Lon,
                            ClientId = clientid,
                        };

                        var address = AddressObject.CreateAddress(a);
                        p.AddressId = address.AddressId;
                    }
                    
                    var created = PlacesObject.CreatePlace(p);
                    
                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Place\": " + JsonConvert.SerializeObject(created, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}