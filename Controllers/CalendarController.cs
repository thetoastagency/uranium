﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Calendar;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class CalendarController : ApiController
    {
        [HttpGet]
        [GET("calendar")]
        public List<CalendarWrapper> Get([FromUri]int[] talent, [FromUri]long date, [FromUri]int days = 30)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                var start = Helper.FromUnixTime(date);
                var end = start.AddDays(days);

                var span = Helpers.DayGenerator.Generate(start, end);
                var calendar = new List<CalendarMeta>();
                var wrapper = new List<CalendarWrapper>();
                var count = 1;

                foreach (var t in talent)
                {
                    calendar.AddRange(CalendarObject.GetCalendarEntries(t, client, start, end));
                }
                    

                foreach(var day in span)
                {
                    var matches = calendar.Where(x => x.Starting.ToShortDateString() == day.ToShortDateString()).ToList();
                    var e = new DateBlob {Friendly = day.ToShortDateString(), Epoch = day.ToUnixTime()};
                    var w = new CalendarWrapper { Date = e, Day = count, Entries = matches };
                            
                    count = count + 1;
                    wrapper.Add(w);
                }

                return wrapper;
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("calendar-types")]
        public IEnumerable<CalendarType> GetCalendarTypes()
        {
            try
            {
                return CalendarObject.GetCalendarTypes();
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("calendar-types/{id:int}")]
        public CalendarType GetCalendarTypeById(int id)
        {
            try
            {
                return CalendarObject.GetCalendarType(id);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("calendar/entries/{type:int}/appearance/{id:int}")]
        public AppearanceMeta GetAppearance(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return AppearanceObject.GetAppearance(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpGet]
        [GET("calendar/entries/{type:int}/hotelstay/{id:int}")]
        public HotelMeta GetHotelStay(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return HotelObject.GetHotelStayWrapped(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpGet]
        [GET("calendar/entries/{type:int}/flight/{id:int}")]
        public FlightMeta GetFlight(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return FlightObject.GetFlight(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpGet]
        [GET("calendar/entries/{type:int}/transport/{id:int}")]
        public TransportMeta GetTransport(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return TransportObject.GetTransport(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }

        [HttpGet]
        [GET("calendar/entries/{type:int}/dinner/{id:int}")]
        public DinnerMeta GetDinner(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return DinnerObject.GetDinner(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }

        }
    }
}