﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Calendar;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Filters;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class FlightsController : ApiController
    {
        [HttpGet]
        [GET("flights")]
        public IEnumerable<FlightMeta> Get()
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return FlightObject.GetFlights(client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [NullObjectActionFilter]
        [GET("flights/{id}")]
        public FlightMeta GetFlightById(int id)
        {
            try
            {
                var client = (int)ControllerContext.RouteData.Values["client-id"];
                return FlightObject.GetFlight(id, client);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("flights/{id}")]
        public HttpResponseMessage UpdateFlight([FromBody]FlightPayload payload, int id)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var departure = Helper.FromUnixTime(payload.DepartureDateTime);
                    var arrival = Helper.FromUnixTime(payload.ArrivalDateTime);

                    var flight = new Flight
                    {
                        ClientId = clientid,
                        FlightNumber = payload.FlightNumber,
                        ArrivalAirportId = payload.ArrivalAirportId,
                        ArrivalTerminal = payload.ArrivalTerminal,
                        AirlineId = payload.AirlineId,
                        DepartureAirportId = payload.DepartureAirportId,
                        DepartureTerminal = payload.DepartureTerminal,
                        FlightId = id,
                        Notes = payload.Notes,
                        TravelAgent = payload.TravelAgent
                    };

                    var ll = new List<FlightPax>();
                    if(payload.Passengers.Count() > 0)
                    {
                        foreach(var passenger in payload.Passengers)
                        {
                            var pax = new FlightPax
                            {
                                Class = passenger.Class,
                                Cost = passenger.Cost,
                                Currency = passenger.CurrencyId,
                                FlightId = id,
                                Locator = passenger.Locator,
                                Meal = passenger.Meal,
                                Mileage = passenger.Mileage,
                                Notes = passenger.Notes,
                                Passenger = passenger.Passenger,
                                Seat = passenger.Seat,
                                Ticket = passenger.TicketNumber
                            };
                            ll.Add(pax);
                        }
                    }
                    else
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("[{\"message\":\"Payload must include passengers.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var updated = FlightObject.UpdateFlight(flight, ll, departure, arrival, payload.TalentId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.FlightId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("flights")]
        public HttpResponseMessage CreateFlight([FromBody]FlightPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                    var departure = Helper.FromUnixTime(payload.DepartureDateTime);
                    var arrival = Helper.FromUnixTime(payload.ArrivalDateTime);

                    var flight = new Flight
                    {
                        ClientId = clientid,
                        ArrivalAirportId = payload.ArrivalAirportId,
                        ArrivalTerminal = payload.ArrivalTerminal,
                        AirlineId = payload.AirlineId,
                        DepartureAirportId = payload.DepartureAirportId,
                        DepartureTerminal = payload.DepartureTerminal,
                        Notes = payload.Notes,
                        FlightNumber = payload.FlightNumber,
                        TravelAgent = payload.TravelAgent
                    };

                    var ll = new List<FlightPax>();
                    if (payload.Passengers.Count() > 0)
                    {
                        foreach (var passenger in payload.Passengers)
                        {
                            var pax = new FlightPax
                            {
                                Class = passenger.Class,
                                Cost = passenger.Cost,
                                Currency = passenger.CurrencyId,
                                FlightId = payload.FlightId,
                                Locator = passenger.Locator,
                                Meal = passenger.Meal,
                                Mileage = passenger.Mileage,
                                Notes = passenger.Notes,
                                Passenger = passenger.Passenger,
                                Seat = passenger.Seat,
                                Ticket = passenger.TicketNumber
                            };
                            ll.Add(pax);
                        }
                    } 
                    else
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("[{\"message\":\"Payload must include passengers.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var updated = FlightObject.CreateFlight(flight, ll, departure, arrival, payload.TalentId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.\", \"Id\": " + JsonConvert.SerializeObject(updated.FlightId, Formatting.None) + "}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }
    }
}