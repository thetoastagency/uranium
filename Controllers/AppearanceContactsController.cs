﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Contact;
using AttributeRouting.Web.Http;
using Uranium.Authorize;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class AppearanceContactsController : ApiController
    {
        [HttpGet]
        [GET("appearances/{appearance}/contacts")]
        public List<LiteContact> GetContactsLinkedToAppearance(int appearance)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                return AppearanceContactsObject.GetAppearanceContactsMarshalled(appearance, clientid);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpPost]
        [POST("appearances/{appearance}/contacts/")]
        public HttpResponseMessage AddContactToAppearance([FromBody]AppearanceContactPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];

                    if (!ClientObject.HasClientAccessToAppearance(clientid, payload.AppearanceId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    AppearanceContactsObject.CreateAppearanceContact(payload.AppearanceId, payload.ContactId);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpDelete]
        [DELETE("appearances/{appearance}/contacts/{contact}")]
        public HttpResponseMessage DeleteAppearanceContact(int appearance, int contact)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];

                if (!ClientObject.HasClientAccessToAppearance(clientid, appearance))
                {
                    var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                    {
                        Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                    };
                    throw new HttpResponseException(bad);
                }

                AppearanceContactsObject.DeleteAppearanceContact(appearance, contact);

                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"message\":\"Success.}]")
                };
                return happy;

            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}