﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    public class AirportsController : ApiController
    {
        [HttpGet]
        [GET("airports")]
        public IEnumerable<Airport> Get(int from = 0, int wanted = 50)
        {
            try
            {
                return (AirportObject.GetAirportsPaged(from, wanted)).Select(x => x);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("airports/{icao}")]
        public Airport GetAirportById(string icao)
        {
            try
            {
                return AirportObject.GetAirport(icao);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}