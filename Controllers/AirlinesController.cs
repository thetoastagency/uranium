﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Uranium.Authorize;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    public class AirlinesController : ApiController
    {
        [HttpGet]
        [GET("airlines")]
        public IEnumerable<Airline> Get(int from = 0, int wanted = 50)
        {
            try
            {
                return (AirlineObject.GetAirlinesPaged(from, wanted)).Select(x => x);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("airlines/{iata}")]
        public Airline GetAirlineById(string iata)
        {
            try
            {
                return AirlineObject.GetAirline(iata);
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}