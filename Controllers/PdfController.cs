﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicCore.Model.Invoice;
using AttributeRouting.Web.Http;
using Uranium.Authorize;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class PdfController : ApiController
    {
        [HttpGet]
        [GET("pdf/invoice/appearance/{id}")]
        public HttpResponseMessage GenerateInvoice(int id)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                var user = (int)ControllerContext.RouteData.Values["user-id"];
                var client = ClientObject.GetClient(clientid);
                var invoice = AppearanceObject.GenerateInvoice(id, clientid, user);

                if (string.IsNullOrEmpty(invoice.Purchaser))
                    throw new Exception("Cannot generate invoice when Purchaser is empty.");

                if (invoice.PurchaserAddress == null)
                    throw new Exception("Cannot generate invoice when Purchaser Address is empty.");

                if (invoice.Items.Count() == 0)
                    throw new Exception("Cannot generate an empty invoice where no entries exist.");

                var pdf = Pdf.GenerateInvoice(invoice, client);

                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"LocalPath\":\"" + pdf + "\", \"Generated\":\"" + DateTime.UtcNow + "\"}]")
                };
                return happy;
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("pdf/agreement/appearance/{id}")]
        public HttpResponseMessage GenerateAgreement(int id, [FromUri]int selectedTemplate)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                var client = ClientObject.GetClient(clientid);
                var agreement = AppearanceObject.GenerateAgreement(id, clientid, selectedTemplate);
                var pdf = Pdf.GenerateAgreement(agreement, client);

                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"LocalPath\":\"" + pdf + "\", \"Generated\":\"" + DateTime.UtcNow + "\"}]")
                };
                return happy;
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("pdf/itinerary/{talent}")]
        public HttpResponseMessage GenerateItinerary(int talent, [FromUri]long from, [FromUri]long to)
        {
            try
            {
                var clientid = (int)ControllerContext.RouteData.Values["client-id"];
                var start = Helper.FromUnixTime(from);
                var end = Helper.FromUnixTime(to);

                var client = ClientObject.GetClient(clientid);
                var itinerary = ItineraryObject.GenerateItinerary(start, end, talent, clientid);

                var pdf = Pdf.GenerateItinerary(itinerary, client);

                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"LocalPath\":\"" + pdf + "\", \"Generated\":\"" + DateTime.UtcNow + "\"}]")
                };
                return happy;
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}