﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using AtomicCore;
using AtomicData;
using AttributeRouting.Web.Http;
using Newtonsoft.Json;
using Uranium.Authorize;
using Uranium.Models;
using Exception = System.Exception;

namespace Uranium.Controllers
{
    [BasicHttpAuthorize(RequireAuthentication = true)]
    [SsoHandler]
    public class AccountsController : ApiController
    {
        [HttpGet]
        [GET("account/{appearance}")]
        public HttpResponseMessage GetAccount(int appearance)
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                var client = UserObject.GetUser(token).ClientId;

                if (!ClientObject.HasClientAccessToAppearance(client, appearance))
                {
                    var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                    {
                        Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                    };
                    throw new HttpResponseException(bad);
                }

                var app = AppearanceObject.GetAppearance(appearance, client);
                var account = AccountObject.GetAccount(client, appearance);
                var response = new StringBuilder();

                foreach (var acc in account)
                {
                    _buildResponse(response, acc);
                }

                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"AppearanceId\":\"" + app.AppearanceId + "\", \"TalentId\":\"" + app.TalentId + "\", \"Account\": [" + response + "]}]")
                };
                return happy;

            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpGet]
        [GET("account/balance/{appearance}")]
        public BalancePayload GetAccountBalance(int appearance)
        {
            try
            {
                var token = ControllerContext.RouteData.Values["user-guid"].ToString();
                var client = UserObject.GetUser(token).ClientId;
                var account = AccountObject.GetAccountBalance(client, appearance);
                var bp = new BalancePayload
                    {
                        Balance = account.Balance,
                        Currency = account.Currency
                    };
                return bp;
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }

        [HttpDelete]
        [DELETE("account")]
        public HttpResponseMessage DeleteAccount([FromBody]AccountPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int) ControllerContext.RouteData.Values["client-id"];
                    
                    if (!ClientObject.HasClientAccessToAccount(clientid, payload.AccountId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var account = new Account
                    {
                        AccountId = payload.AccountId,
                        AppearanceId = payload.AppearanceId,
                        ClientId = clientid,
                        FeeType = payload.FeeType
                    };

                    AccountObject.DeleteAccountEntry(account);

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"message\":\"Success.}]")
                    };
                    return happy;

                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("account")]
        public HttpResponseMessage CreateAccount([FromBody]AccountPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];

                    if (!ClientObject.HasClientAccessToAppearance(clientid, payload.AppearanceId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var appearance = AppearanceObject.GetAppearance(payload.AppearanceId, clientid);
                    var account = new Account
                        {
                            AppearanceId = appearance.AppearanceId,
                            ClientId = clientid,
                            ContactId = appearance.Appearance.Purchaser.ContactId,
                            TalentId = appearance.TalentId,
                            FeeType = 1,
                            Currency = payload.Currency,
                            Description = payload.Description,
                            Credit = 0,
                            Debit = (decimal) payload.Amount,
                            Ref = appearance.AppearanceId,
                            Locating = @"appearance",
                            Locator = appearance.AppearanceId,
                            TaxApplicable = payload.TaxApplicable,
                            TaxInclusive = payload.TaxInclusive,
                            TaxRate = payload.TaxRate
                        };

                    var created = AccountObject.CreateAccount(account);
                    var response = new StringBuilder();

                    foreach(var acc in created)
                    {
                        _buildResponse(response,acc);
                    }

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"AppearanceId\":\"" + appearance.AppearanceId + "\", \"TalentId\":\"" + appearance.TalentId + "\", \"Account\": [" + response + "]}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        [HttpPost]
        [POST("account/{appearance}")]
        public HttpResponseMessage AddToAccount([FromBody]AccountPayload payload)
        {
            if (ModelState.IsValid && payload != null)
            {
                try
                {
                    var clientid = (int)ControllerContext.RouteData.Values["client-id"];

                    if (!ClientObject.HasClientAccessToAppearance(clientid, payload.AppearanceId))
                    {
                        var bad = new HttpResponseMessage(HttpStatusCode.Forbidden)
                        {
                            Content = new StringContent("[{\"message\":\"You do not have access to that.}]")
                        };
                        throw new HttpResponseException(bad);
                    }

                    var appearance = AppearanceObject.GetAppearance(payload.AppearanceId, clientid);
                    var loc = new Locator();
                    
                    switch (payload.FeeType)
                    {
                        case 3:
                            loc.Ref = payload.RefId;
                            loc.Locate = @"hotelstay";
                            loc.Locating = payload.RefId;
                            break;
                        case 2:
                            loc.Ref = payload.RefId;
                            loc.Locate = @"flight";
                            loc.Locating = payload.RefId;
                            break;
                        case 8:
                            loc.Ref = payload.RefId;
                            loc.Locate = @"taxi";
                            loc.Locating = payload.RefId;
                            break;
                        default:
                            loc.Ref = appearance.AppearanceId;
                            loc.Locate = @"appearance";
                            loc.Locating = appearance.AppearanceId;
                            break;
                    }
                    
                    var account = new Account
                    {
                        AppearanceId = appearance.AppearanceId,
                        ClientId = clientid,
                        ContactId = appearance.Appearance.Purchaser.ContactId,
                        TalentId = appearance.TalentId,
                        FeeType = payload.FeeType,
                        Currency = payload.Currency,
                        Description = payload.Description,
                        Ref = loc.Ref,
                        Locating = loc.Locate,
                        Locator = loc.Locating,
                        TaxApplicable = payload.TaxApplicable,
                        TaxInclusive = payload.TaxInclusive,
                        TaxRate = payload.TaxRate
                    };

                    var ft = FeeTypeObject.GetFeeType(payload.FeeType);
                    if(ft.Income)
                    {
                        account.Debit = payload.Amount;
                        account.Credit = 0;
                    } 
                    else
                    {
                        account.Credit = payload.Amount;
                        account.Debit = 0;
                    }

                    var created = AccountObject.AddEntryToAccount(account);
                    var response = new StringBuilder();

                    var length = created.Count();
                    var i = 0;
                    var last = false;

                    foreach (var acc in created)
                    {
                        if(i == (length - 1))
                        {
                            last = true;
                        }
                        _buildResponse(response, acc, last);
                        i++;
                    }

                    var happy = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent("[{\"AppearanceId\":\"" + appearance.AppearanceId + "\", \"TalentId\":\"" + appearance.TalentId + "\", \"Account\": [" + response + "]}]")
                    };
                    return happy;
                }
                catch (Exception ex)
                {
                    var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                    throw new HttpResponseException(e);
                }
            }

            var sad = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent("[{\"message\":\"Payload invalid or missing.}]")
            };
            throw new HttpResponseException(sad);
        }

        private static StringBuilder _buildResponse(StringBuilder sb, Account account, bool last = false)
        {
            sb.Append("{");
            sb.Append("\"AccountId\": " + account.AccountId + ",");
            sb.Append("\"Description\": \"" + account.Description + "\",");
            sb.Append("\"Detail\": \"" + account.Detail + "\",");
            sb.Append("\"Credit\": " + account.Credit + ",");
            sb.Append("\"Debit\": " + account.Debit + ",");
            sb.Append("\"Balance\": " + account.Balance + ",");
            sb.Append("\"Currency\": " + account.Currency + ",");
            sb.Append("\"Ref\": " + account.Ref + ",");
            sb.Append("\"Locator\": \"" + account.Locating + "\"");
            if(account.TaxApplicable)
            {
                sb.Append("\"TaxAmount\": \"" + account.TaxAmount + "\"");
                sb.Append("\"TaxRate\": \"" + account.TaxRate + "\"");
                sb.Append("\"TaxInclusive\": \"" + account.TaxInclusive + "\"");
            }
            
            sb.Append(last ? "}" : "},");
            return sb;
        }
    }
}