﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AtomicCore;
using AttributeRouting.Web.Http;

namespace Uranium.Controllers
{
    public class CacheController : ApiController
    {
        [HttpGet]
        [GET("cache")]
        public HttpResponseMessage Purge()
        {
            try
            {
                CacheManager.Purge();

                var happy = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("[{\"message\":\"Success. Cache purged.\"}]")
                };
                return happy;
            }
            catch (Exception ex)
            {
                var e = Handlers.ExceptionHandler.Throw(ex, HttpStatusCode.InternalServerError);
                throw new HttpResponseException(e);
            }
        }
    }
}