﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;

namespace Uranium.Handlers
{
    public class ExceptionHandler
    {
        public static HttpResponseMessage Throw(Exception exception, HttpStatusCode httpStatusCode)
        {
            var e = new Models.Exception() { Error = exception, Message = exception.Message, Response = httpStatusCode };
            var resp = new HttpResponseMessage(httpStatusCode)
                           {
                               Content = new StringContent(JsonConvert.SerializeObject(e, Formatting.None)),
                               ReasonPhrase = exception.Message.Replace("\r\n", "").Trim()
            };
            return resp;
        }
    }
}