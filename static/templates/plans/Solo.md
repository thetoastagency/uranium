### Just starting out? Brand new agency?

Solo is the plan for you if you're a new agency just starting out or you only represent a single talent.  To ensure you're competitive from your first appearance, Loop charges just 1%* on all your booking fees with no minimum per month.  Once you're setup with your single talent, you get access to all of Loop's features with no restrictions or limitations.