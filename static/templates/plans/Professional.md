### Professional tools for a professional agency

Pro is our most popular plan. Designed for a busy, international agency with a medium-sized roster.  Focus your attention on promoting your talent and expanding into new countries and let Loop manage the daily running of your talent. Loop charges just 2.5%* on all your booking fees with a small standard fee of £50 per month.

Maximum talent managed on this plan is fifteen.