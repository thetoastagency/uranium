### Grow your agency

Standard is the perfect plan for a small, but focussed agency. So your agency grows and you can concentrate on promoting your talent rather than running their itineraries, Loop charges just 1.75%* on all your booking fees with no minimum per month.  You'll have access to all of Loop's fantastic features and tools with just a limit on the number of talent you can manage.

Maximum talent managed on this plan is three.