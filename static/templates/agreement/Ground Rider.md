It is required that the driver of the vehicle be well presented and show identification to the talent on pick-up.

The car must:-

* be of an executive or luxury saloon or SUV type vehicle. Such-as: BMW, Mercedes-Benz, Jaguar, Bentley or Rolls-Royce
* have leather upholstery and adjustable air-conditioning from within the passenger compartment
* be driven in accordance to the local laws and with consideration to the wishes and well-being of the passengers
* have a clean boot/trunk to store the Talent's possessions
* be driven via the most efficient and direct route from pick-up to drop-off.
* have stops must be authorised by the Talent