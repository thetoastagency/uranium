Minimum specification setup:-

* Pioneer DJM-800 Mixer or Ableton Xone:64 Mixer
* 4 Pioneer CDJ MK3s or MK2s (no alternatives acceptable)
* Pioneer EFX-1000 Effects unit
* 3 Vermona Action Filters
* A minimum of 2 monitor speakers; bi-amped full range

The monitors must:-

* not be self-powered
* be rigged at head level or higher with left/right balance configuration (not behind or overhead the playing position)
* controlled via the mixer
* be no further than 2 metres from the Talent's playing position