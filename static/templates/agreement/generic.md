##### (1) Transportation #####

1.1 Where applicable, all International transportation shall be booked by     the Agency and reimbursed by the Purchaser no-later than four weeks prior to the Performance Date.

1.2 Agency has right to cancel the Performance and retain any deposit paid in full should transportation costs remain outstanding two-weeks prior to the arranged departure date.

##### (2) Accommodation #####

2.1 The Purchaser shall confirm and pay for a hotel of the Talent's choice for one (1) night under the Talent's name(s).

2.2 The Purchaser shall pre-pay on account the Talent's entire 
hotel expenses excluding telephone calls and alcoholic beverages. 

2.3 Where applicable, the Purchaser agrees to arrange for a late check-out for two (2) hours prior to the Talent’s departure time 
on the day following the Performance. 

2.4 In the event that the Talent require two (2) nights, the Purchaser will cover the cost of the additional night.

##### (3) Ground Transportation #####

3.1 The Purchaser agrees to book and pre-pay for all ground transportation where required, and confirm these arrangements with the Agency.  When the Talent is travelling with a Tour Manager, the Purchaser agrees to pay for an additional car for him/her.


##### (4) Visas & Work Permits #####

4.1 The Purchaser agrees to secure all necessary documentation and work permits for Talent's legal Performance. Purchaser agrees to pay in full any and all costs of securing such documents in advance of Talent's arrival.

4.2 If Purchaser has not appropriately secured such documents for Talent's Performance, the Agency and/or Talent reserve the right to cancel the scheduled Performance without notice to the Purchaser.

##### (5) Riders #####

5.1 The riders are separate documents to this agreement, but each, must be signed and dated along with this contractual document and is to be respected as part of the entire Performance Agreement.  Failure to comply with the terms within the riders shall constitute a material breach of this Performance Agreement.

5.2 Any Performance Agreement returned to the Agency without all riders attached, signed and dated shall render the Performance Agreement null and void and shall constitute due cause for immediate cancellation by the Talent and/or Agency of the Performance or, where applicable, Performances, as specified in this Performance Agreement.

5.3 No Performance Agreement shall be binding until executed by the Talent.

##### (6) Venue #####

6.1 The Performance shall take place at the Venue, unless otherwise specifically agreed by the Agency.

##### (7) Performance Time #####

7.1 The Purchaser agrees to confirm and mutually agree the Talent's Performance time and length two (2) weeks prior to the Performance.  The Talent agrees to perform for a maximum of two (2) hours unless previously agreed with the Agency.

##### (8) Billing & Advertising #####

8.1 The Talent shall receive headline billing in all advertising and publicity including, but not limited to; air time, newspaper and trade-advertisements, flyers, e-flyers, billboards and marquees unless otherwise agreed in the attached Marketing Rider, if such is attached.

8.2 All flyers and artwork must be approved by the Agency prior to going to print. Failure to gain approval from the Agency will result in a material breach of this Performance Agreement.

8.3 The Talent's name may not be used or associated directly or indirectly with any product or service without the consent of the Talent.

8.4 Any photographs, biographical information and/or other material supplied by the Talent or Agency to the Purchaser shall be, and remains, the property of the Talent.

8.5 The Purchaser agrees to not commence advertising or promotion of this Performance hereunder without confirmation of the Performance from the Agency.

8.6 Talent shall be under no obligation to give any interviews to radio, television or press throughout the term of this Performance Agreement, save where the Talent has given prior
permission.


##### (9) Security #####

9.1 The Purchaser agrees to appoint two (2) security personnel to escort the Talent into the Venue.  The Purchaser agrees to employ an adequate number of security personnel who shall protect the Talent and Talent's personal property during and after the Performance.

9.2 The Purchaser shall ensure the stage/booth is kept free of any persons other than those directly involved with the Performance throughout said Performance.

9.3 The Purchaser shall be responsible and liable for all damages, not incurred by the Talent, to the Talent's (or Tour Manager's) property or equipment due to lack of adequate security or sufficient structural setup of the stage/booth area.

9.4 The Purchaser shall ensure that security shall not use any undue force either word or deed and that no injuries shall be inflicted upon the guests at any time.

##### (10) Recordings #####

10.1 No recordings shall be made, sold or distributed in connection with the Performance.  The recording of the Talent's Performance by any means and in any medium without the Talent's prior consent is strictly prohibited.

10.2 The Purchaser agrees to take all necessary precautions to ensure that no sound or audio-visual recording of any kind is made without the permission of the Talent.

10.3 The Purchaser agrees that no transmission, broadcast by radio, or through the internet, or television or closed circuit television recording, direct filming or other professional recording method will be permitted without the Talent's prior consent.

10.4 If the Venue has any kind of video monitoring systems, it shall not be used on the stage/booth without the Talent's prior consent.

10.5 The Purchaser agrees to pay a penalty of an additional 50% of the Total Performance Fee if any form of unauthorised recording has taken place.

##### (11) Purchaser, Validity & Execution #####

11.1 The Purchaser or his authorised representative shall be present from the Talent's arranged Performance time throughout the Performance.

11.2 The signatory executing this Performance Agreement for, or as, the Purchaser's behalf warrants his authority to do so, that he has the right to enter into this agreement and is of legal age and hereby personally assumes liability for payment of all compensation, Performance, and all Purchaser's obligations hereunder.

##### (12) Cancellation #####

12.1 Notwithstanding anything to the contrary contained within this Performance Agreement:

12.2 The Talent reserves the right to terminate the Performance at any time up-to ten (10) weeks prior to the Performance Date and agrees:

12.2.1 to repay to the Purchaser any sums paid save for non-refundable expenses already incurred such-as flights and accommodations;

12.2.2 to repay to the Purchaser any sums paid save for out of pocket expenses already incurred by the Talent or Agency;

12.3 The Talent shall not be required to render services other than that on the Performance Date and at the Venue as set-out in this Performance Agreement.  The Purchaser shall be liable for full payment of the Total Performance Fee hereunder and any other sums incurred in collection of Total Performance Fee to be paid.

12.4 If any payments required to be made by the Purchaser hereunder that are not made in a timely manner, including a five (5) day cure period for the Purchaser to make such payments), the Agency reserve the right, without notice, to cancel this Performance Agreement and retain all monies previously paid hereunder. In addition:

12.4.1 if Talent, or Agency have, at the time of such termination, incurred costs in relation to this Performance Agreement the Purchaser will reimburse said costs.

12.5 If termination of the Performance occurs greater than four (4) weeks prior to the Performance Date, and within eight (8) weeks of the Performance Date, the Agency shall be
entitled to retain fifty (50) percent of fees paid to the Agency.

12.6 If termination of the Performance occurs within four (4) weeks of the Performance Date, the Agency shall be entitled to retain one hundred (100) percent of fees paid to the Agency.


##### (13) Insurance #####

13.1 The Purchaser shall provide and maintain, valid and comprehensive general liability insurance as required by the laws governing the Venue.

13.2 The Purchaser hereby agrees to indemnify the Talent and Agency against any third-party claims howsoever they may arise whilst Talent is involved in contractual duties.

##### (14) Merchandising #####

14.1 The Talent reserves the right to sell CDs, DVDs, videos and/or other merchandising material at the Venue and retain all the proceeds.

14.2 If the Purchaser uses the Talent's name or likeness on any Performance merchandising, the Talent shall be entitled to fifteen (15) percent of the resulting proceeds.

##### (15) Force Majeure #####

15.1 If, up-to ten (10) weeks prior to the Performance Date hereunder, the Performance becomes impracticable as a consequences of Force Majeure, such-as, but not limited to; public calamity, political or labour unrest, armed conflict, epidemic, fire, flood or other act of God, then this Performance Agreement can be terminated, without notice, and neither
party shall be liable to the other, save where set-out in 15.2

15.2 If the Performance is terminated as per 15.1, and the Agency has incurred costs in connection with this Performance hereunder, the Purchaser agrees to repay promptly all costs incurred.
These include, but are not limited to, flights, accommodation, ground transportation and non-refundable costs.

15.3 If, within ten (10) weeks of the Performance Date hereunder, the Performance becomes impracticable as a consequence of Force Majeure, such-as, but not limited to; public calamity, political or labour unrest, armed conflict, epidemic, fire, flood or other act of God, then this Performance Agreement can be terminated, with notice to both parties. The Purchaser shall be liable for costs as set-out in 15.4 and 15.5

15.4 If the Performance is terminated as per 15.3, and the Agency has incurred costs in connection with this Performance hereunder, the Purchaser agrees to repay promptly all costs incurred.
These include, but are not limited to, flights, accommodation, ground transportation and non-refundable costs.

15.5 If the Performance is terminated as per 15.3, the Agency shall be entitled to retain fifty (50) percent of fees paid to the Agency and the Agency shall return the remain fifty (50) percent
of such fees, subject to 15.4, to the Purchaser.

##### (16) Breach #####

16.1 Failure by the Purchaser to comply with any terms within this Performance Agreement shall constitute a material breach of contract based upon which the Talent shall be entitled to cancel the Performance without prejudice to payment of the Total Performance Fee and any other costs, without notice.

16.2 If the Purchaser is unable to meet any of the requirements stated in any riders, the Purchaser shall be liable for any fees, charges or remuneration incurred by the Talent to obtain such services or items.

16.3 If the Performance is terminated as per 16.1 and one hundred (100) percent of the Total Performance Fee has been paid, the Agency shall be entitled to retain one hundred (100) percent of said fees.

##### (17) Interpretation #####

17.1 This Performance Agreement shall not be interpreted or construed to constitute a partnership, joint-venture, agency or employer/employee relationship between Purchaser and Talent.

17.2 No modification, amendment, waiver, termination or discharge of this Performance Agreement shall be binding upon the Talent unless confirmed by written instrument signed by the Talent or Talent's authorised representative.

17.3 No waiver of any provision or any default under this Performance Agreement shall constitute a waiver by the Talent or compliance thereafter with the same or any other provision or of the Talent's right to enforce the same or any other provision thereafter.

17.4 If Purchaser is comprised of more than one individual, the terms and conditions set forth in this Performance Agreement shall apply to member of the Purchaser jointly and severally.

17.5 If any clause, part or rider within this Performance Agreement is determined to be unenforceable by a court or tribunal of competent jurisdiction to make such a determination, the remainder of this Performance Agreement shall remain in full force and effect.

17.6 If any provision, which in the Talent's sole option, is rendered unenforceable in court or tribunal of competent jurisdiction to make such a determination, the Talent shall have the option to cancel the Performance without incurring any liability to the Purchaser or any third party.

17.7 This Performance Agreement contains the entire understanding between Purchaser and Talent; it supersedes any and all other prior agreements, correspondence or statements, whether
written or verbal.