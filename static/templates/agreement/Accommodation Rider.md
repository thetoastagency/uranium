All accommodation must be confirmed with the agency prior to booking.  Making a booking without confirmation will not be accepted unless agreed prior.

Where applicable, accommodation must-be at least 4-class standard with lockable rooms, private bathrooms and access to a safety deposit box or safe.  Room must be of suite type where possible and have a minimum sized queen double bed.

Suites with multiple bedrooms can be booked to accommodate all persons with Talent, but only on approval of the Talent.  

Rooms booked must be comparable for each persons within Talent.

Where possible, the Purchaser will request a high-floor and ensure that late check-out has been arranged and funded for departure day.