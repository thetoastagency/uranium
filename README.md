=== All End Points ===

==List All==

* {{{/routes.axd}}} - List all available endpoints.

== Authentication ==

* {{{/login}}} - On success, returns block containing sso-token and other user details. Pass email and password within the request payload as {{{application/json}}}.

== Account ==

Account is a financial record for an appearance. More details regarding use of these endpoints in [[AccountEndpoints|AccountEndpoints]].

* {{{/account/{appearance}}}} - Returns all financial entries within account for given appearance. It cannot be paged.
* {{{/account/balance/{appearance}}}} - Returns balance of account for appearance. Balance is returned as float.
* {{{/account}}} = Posting a creation [[AccountPayload|AccountPayload]] creates a new account.
* {{{/account/{appearance}}}} - Posting [[AccountPayload|AccountPayload]] adds a new entry to account.

== Address ==
* {{{/address/{id}}}} - Returns address for given id.

== Advisories ==

* {{{/advisories/{id}}}} - Returns unread advisories for user. It does not mark them as read.

== Agreements ==
Read [[AgreementPayload|AgreementPayload]] for explanation to what the hell Agreements are.

* {{{/agreements}}} - Returns all agreements.
* {{{/agreements/{id}}}} - Returns a given agreement. Id is an int.
* {{{/agreements}}} - Posting [[AgreementPayload|AgreementPayload]] will create a new agreement.
* {{{/agreements/{id}}}} - Posting [[AgreementPayload|AgreementPayload]] will update an existing agreement.

== Airlines ==

Airlines is an unsecured set of end-points. The transmission of an SSO-Token is not required.

* {{{/airlines}}} - Returns all airlines. Accepts from and wanted params to provide paged list of airlines. From is 'from record id', Wanted is 'number of records from //from//'.
* {{{/airlines/{iata}}}} - Returns given airline. IATA code is a 2 character code for the airline (BA is British Airways for example).

==Airports==

Airports is an unsecured set of end-points.  The transmission of an SSO-Token is not required.

* {{{/airports}}} - Returns all airports. Accepts from and wanted params to provide paged list of airlines. From is 'from record id', Wanted is 'number of records //from//'.
* {{{/airports/{icao}}}} - Returns given airport. ICAO code is a 4 character code for the airport (EGLL is Heathrow, KJFK is Kennedy International for example).

==Appearances==

* {{{/appearances}}} - Returns all appearances for client (agency).  Does not accept paging.
* {{{/appearances/{id}}}} - Returns full appearance including all misc data. See [[AppearanceResponse|AppearanceResponse]].
* {{{/appearances}}} - Posting [[AppearancePayload|AppearancePayload]] will create an appearance and return an AppearanceId and CalendarId, the latter on the whole can be ignored.
* {{{/appearances/{id}}}} - Posting [[AppearancePayload|AppearancePayload]] will update the given appearance where the payload contains a valid AppearanceId.  Be aware that appearance type, talent and fee model (exclusive or landed) cannot be changed.

== Appearance Contacts ==

* {{{/appearances/{id}/contacts}}} - Returns a list of all contacts associated with the appearance given in the ID.
* {{{/appearances/{id}/contacts/{contact}}} - Attempts to delete the contact from the given appearance.  In the case of the Purchaser, this will not remove a purchaser from the appearance, but remove his association, which is largely academic in this case.
* {{{/appearances/{id}/contacts}}} - Posting [[AppearanceContactsPayload|AppearanceContactsPayload]] will create the contact to appearance association.  This is used for itinerary purposes primarily, but will also be beneficial for reporting.

== Artist ==

* {{{/artists}}} - Returns list of artists for given client (agency).
* {{{/artists/{id}}}} - Returns given artist.

There are no POST endpoints for artist because the Contact endpoints should be used. See [[ContactPayload|ContactPayload]].

== Calendar ==

* {{{/calendar}}} - Providing the talent, date and day parameters on the URI, will return a full list of dates, where a date contains an event (appearance/flight/hotel stay etc.) these will be returned. See [[CalendarRequestPayload|CalendarRequestPayload]] for example of request.  See [[CalendarResponse|CalendarResponse]] for example of response.
* {{{/calendar/types}}} - Returns a list of all calendar types (flight, appearance, meeting etc.)
* {{{/calendar/types/{id}}}} - Returns a single calendar type where an id is provided.
* {{{/calendar/entries/{type:int}/flight/{id:int}}}} - Passing [[CalendarTypeId|CalendarTypeId]] and id of requested flight will return [[CalendarEntryFlight|CalendarEntryFlight]] payload.
* {{{/calendar/entries/{type:int}/hotelstay/{id:int}}}} - Passing [[CalendarTypeId|CalendarTypeId]] and id of requested hotel stay will return [[CalendarEntryHotel|CalendarEntryHotel]] payload.
* {{{/calendar/entries/{type:int}/appearance/{id:int}}}} - Passing [[CalendarTypeId|CalendarTypeId]] and id of requested flight will return [[CalendarEntryAppearance|CalendarEntryAppearance]] payload.
* {{{/calendar/entries/{type:int}/transport/{id:int}}}} - Passing [[CalendarTypeId|CalendarTypeId]] and id of requested trip will return [[CalendarEntryTransport|CalendarEntryTransport]] payload.
* {{{/calendar/entries/{type:int}/dinner/{id:int}}}} - Passing [[CalendarTypeId|CalendarTypeId]] and id of requested dinner appointment will return [[CalendarEntryDinner|CalendarEntryDinner]] payload.

== Cities ==

Cities is an unsecured set of end-points. The transmission of an SSO-Token is not required.

* {{{/cities}}} - Returns all cities. Accepts from and wanted params to provide paged list of cities.
* {{{/cities/{id}}}} - Returns given city.

== Contacts ==

* {{{/contact}}} - Posting [[ContactPayload|ContactPayload]] will create contact in the database.  Type property is not required within payload.  System will create 'UNSPECIFIED' contact type.
* {{{/contacts}}} - Returns all contacts as light list, with rolodex style grouping. Contacts that have details will have primary contact details returned. Accepts from and wanted params to provide paged list of contacts.
* {{{/contacts?filter={x}}}} - Passing type (int) will return all contacts matching that type.
* {{{/contacts/{id}}} - POSTing to this will attempt to update the contact you are posting to. Read details of payload [[Updating%20Contacts%20Payload|Here]].
* {{{/contacts/address/{id}}} - Returns a list of addresses for the given contact. Will only return addresses the client has access to, otherwise an empty array will return.

== Contracting Companies ==

* {{{/contracting-companies}}} - Returns a list of all contracting companies available for the client.
* {{{/contracting-companies}}} - Posting [[ContractingCompanyPayload|ContractingCompanyPayload]] will create a contracting company. Returns the created contracting company object.
* {{{/contracting-companies/{company}}}} - Delete will attempt to delete the contracting company. Remember, the record is *not* deleted, but hidden from future requests. It can be 'undeleted' by performing an update passing the active bool as true.
* {{{/contracting-companies/{company}}}} - Posting [[ContractingCompanyPayload|ContractingCompanyPayload]] will update the contracting company set by the id. Returns the updated contracting company object.

== Continents ==

Continents is an unsecured set of end-points. The transmission of an SSO-Token is not required.

* {{{/continents}}} - Returns all continents.
* {{{/continents/{id}}}} - Returns given continent.

== Countries ==

Countries is an unsecured set of end-points. The transmission of an SSO-Token is not required.

* {{{/countries}}} - Returns all countries. Accepts from and wanted params to provide paged list of countries.
* {{{/countries/{id}}}} - Returns given country.

== Dinner ==

* {{{/dinner}}} - Returns a list of dinners for client. Not really needed publically.
* {{{/dinner/{id}}}} - Returns a given dinner instance in the [[DinnerRespones|DinnerResponse]].
* {{{/dinner/{id}}}} - Posting [[DinnerPayload|DinnerPayload]] will update the dinner appointment and calendar entry.
* {{{/dinner}}} - Posting [[DinnerPayload|DinnerPayload]] will create the new dinner appointment and calendar entry.

== Flights ==

* {{{/flight}}} - Returns all flights for client.  Returns as [[FlightsPayload|FlightsPayload]].
* {{{/flights/{id}}}} - Returns given flight. ID is integer.
* {{{/flights}}} - POSTing [[FlightsRequestPayload|FlightsRequestPayload]] will create a flight and calendar entry.
* {{{/flights/{id}}}} - Put Flights Request Payload will update flight.

== Hotel Stays ==

* {{{/hotelstays}}} - Returns all hotel stays for client as [[HotelResponsePayload|HotelResponsePayload]].
* {{{/hotelstays/{id}}}} - Returns a given hotel stay for client. ID is integer.
* {{{/hotelstays}}} - Posting [[HotelRequestPayload|HotelRequestPayload]] will create hotel stay and calendar entry.
* {{{/hotelstays/{id}}}} - Put Hotel Request Payload will update hotel.

== PDF ==

PDF endpoints will generate a PDF for the requested type.

* {{{/pdf/invoice/appearance}}} - Generates an invoice for a given appearance. You must pass [[InvoiceRequestPayload|InvoiceRequestPayload]].

==Places==

* {{{/places/search/{what}/{city}}}} - Returns a list of places such as venues etc. based on {what} search parameter. City is integer representing CityId. Data is munge of Foursquare and atomic data. What should be Urlencoded when transmitted.  See [[ExamplePlaceSearch|ExamplePlaceSearch]].
* {{{/places/{id}}}} - Returns a given place. Place must exist in atomic data.
* {{{/places}}} - Gets full list of places available to client. Heavy. See [[PlacesWithAddress|PlacesWithAddress]] response example.
* {{{/places}}} - Posting [[PlacesPayload|PlacesPayload]] attempts to store place in atomic data.  This is required for use with other atomic objects. Returns place object of created place. If place already exists, it will be updated.
* {{{/places/{foursquare}}}} - Posting a foursquare venue identifier, will create this place and address within the system. System will only create places that are permitted (such-as nightclubs, hotels, bars etc.) and only in a city that exists. The endpoint will throw if geolocator cannot pin-point your chosen city. Use /places post if necessary. See [[FoursquarePlaceCreation|FoursquarePlaceCreation]] example.

== Riders ==

* {{{/riders}}} - Posting the [[RiderPayload|RiderPayload]] will attempt to create a new rider.
* {{{/riders/{talent}}}} - Passing a TalentId will request all riders for that talent as a collection.
* {{{/riders/{talent}/{id}}}} - Passing a TalentId and requested rider ID will return a single rider.
* {{{/riders/{id}}}} - Posting a [[RiderPayload|RiderPayload]] for a given Rider ID will attempt to update the selected rider.

Riders suffer from some terminology problems, to get a better idea of how riders are structured and how they are supposed to work, have a read of [[Riders101|Riders101]].

==Talents==

*{{{/talents}}} - Returns all talents.
*{{{/talents/{id}}}} - Returns given talent.
*{{{/talents}}} - Posting [[TalentPayload|TalentPayload]] attempts to create talent made-up of Artists given.

==Timezones==

*{{{/timezones}}} - Returns all timezones.
*{{{/timezones/{id}}}} - Returns given timezone.

== Transport ==

* {{{/transport}}} - Returns a list of trips for client. Response is [[TransportResponse|TransportResponse]].
* {{{/transport/{id}}}} - Returns a given trip.
* {{{/transport}}} - Posting [[TransportPayload|TransportPayload]] creates the necessary trip and calendar entry. Pay particular attention to the ```Location``` object within the payload.
* {{{/transport/{id}}}} - Posting [[TransportPayload|TransportPayload]] updates the given trip.

== Users ==

* {{{/user}}} - Posting [[UserPayload|UserPayload]] will create user in the database. The user will only be created if the requesting account (that represented by the SSO) is a master account.
* {{{/users}}} - Will return list of users for client if the user is a master account. Else returns single account (as users/{id}).
* {{{/users/{id}}}} - Returns given user.

== Misc ==

* {{{/contact-types}}} - Returns a list of all available contact types (Talent, Promoter/Purchaser etc.)
* {{{/contact-detail-types}}} - Returns a list of available detail types. See [[ContactDetailTypes|ContactDetailTypes]] for immediate list.

== Accessing Uranium ==

[[Accessing%20Uranium%20API|Accessing Uranium API]]

== Known Issues ==
* Currently there is no way to delete a talent or artist that has been created by mistake.  The reason the delete is a challenge is because of what could be linked to that entity.  For example, an artist could have a reference to an appearance, accounting information within that appearance and some dates within the calendar. On deletion, what would the user would reasonably expect the appearance, financial and accounting information to do? Would they expect the information to remain? Would they expect that the system know what is safe and not-safe to delete?
* All dates and times within the core are based on UTC. Therefore, if you were using this system in the USA, because the core is in the UK, the dates and times would be UK and not US.  This is an awkward problem because the only solution would be for dates to be passed from the client all the way through to the core.
* Currently the City Import tool doesn't allow the import of cities with the same name within the same country. Therefore there are missing cities through the system.  One solution is to compare the first 4 digits of the lat/lon (e.g. 45.55), but this would be slow. Further thought here required.