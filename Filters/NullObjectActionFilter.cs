﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Uranium.Filters
{
    public class NullObjectActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            object outValue = null;
            actionExecutedContext.Response.TryGetContentValue<object>(out outValue);
            if (outValue == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("[{\"message\":\"NullObjectActionFilter: Not found or inaccessible within this context.\"}]"),
                    ReasonPhrase = "Not found or inaccessible within this context."
                };
                throw new HttpResponseException(resp);
            }

            base.OnActionExecuted(actionExecutedContext);

        }
    }
}