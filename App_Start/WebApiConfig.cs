﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using AttributeRouting.Web.Http.WebHost;
using Newtonsoft.Json;
using Uranium.Handlers;

namespace Uranium
{
    public static class WebApiConfig
    {
        public static void ConfigureApi(HttpConfiguration config)
        {
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            json.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }

        public static void Register(HttpConfiguration config)
        {
            var routes = GlobalConfiguration.Configuration.Routes;
            routes.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new CorsHandler());
        }
    }
}
