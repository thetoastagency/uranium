﻿using System.Collections.Generic;

namespace Uranium.Models
{
    public class FlightPayload
    {
        public int FlightId { get; set; }
        public string FlightNumber { get; set; }
        public int CalendarId { get; set; }
        public string AirlineId { get; set; }
        public string ArrivalAirportId { get; set; }
        public string DepartureAirportId { get; set; }
        public string ArrivalTerminal { get; set; }
        public string DepartureTerminal { get; set; }
        public string Notes { get; set; }
        public int TravelAgent { get; set; }
        public IEnumerable<FlightPaxPayload> Passengers { get; set; }
        public long DepartureDateTime { get; set; }
        public long ArrivalDateTime { get; set; }
        public int TalentId { get; set; }
    }
}