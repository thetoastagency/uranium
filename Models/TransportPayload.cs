﻿namespace Uranium.Models
{
    public class TransportPayload
    {
        public int TransportId { get; set; }
        public int PassengerContactId { get; set; }
        public int TalentId { get; set; }
        public int CurrencyId { get; set; }
        public int PayeeId { get; set; }
        public Location Pickup { get; set; }
        public Location Dropoff { get; set; }
        public string Carrier { get; set; }
        public decimal Cost { get; set; }
        public string Notes { get; set; }
    }
}