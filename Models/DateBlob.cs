﻿namespace Uranium.Models
{
    public class DateBlob
    {
        public string Friendly { get; set; }
        public long Epoch { get; set; }
    }
}