﻿using System.Net;
using Newtonsoft.Json;

namespace Uranium.Models
{
    public class Exception
    {
        [JsonProperty]
        public HttpStatusCode Response { get; set; }

        [JsonProperty]
        public string Message { get; set; }

        [JsonProperty]
        public System.Exception Error { get; set; }
    }
}