﻿using System.Collections.Generic;

namespace Uranium.Models
{
    public class ContractingCompanyPayload
    {
        public int ContractingCompanyId { get; set; }
        public int AddressId { get; set; }
        public string CompanyName { get; set; }
        public bool Active { get; set; }
        public int OperatingCurrency { get; set; }

        public List<FinancialDetailPayload> Details { get; set; }
    }
}