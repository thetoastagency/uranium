﻿namespace Uranium.Models
{
    public class RiderPayload
    {
        public int RiderId { get; set; }
        public int ArtistId { get; set; }
        public int TalentId { get; set; }
        public string Personal { get; set; }
        public string Ground { get; set; }
        public string Transport { get; set; }
        public string Accommodation { get; set; }
        public string Technical { get; set; }
        public string TechnicalDiagram { get; set; }
        public string Misc { get; set; }
        public string Marketing { get; set; }
        public string PreferredAirport { get; set; }
        public string PreferredAirline { get; set; }
        public int PreferredHotel { get; set; }
    }
}