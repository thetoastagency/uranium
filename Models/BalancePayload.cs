﻿namespace Uranium.Models
{
    public class BalancePayload
    {
        public decimal Balance { get; set; }
        public int Currency { get; set; }
    }
}
