﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Uranium.Models
{
    public class TalentsPayload
    {
        [Required]
        [MaxLength(150)]
        public string TalentName { get; set; }

        [Required]
        [MaxLength(5000)]
        public string Bio { get; set; }

        [Required]
        [MaxLength(150)]
        public string Avatar { get; set; }
        public string InvoicePrefix { get; set; }
        public decimal AgencyCut { get; set; }
        public int PreferredCurrencyId { get; set; }
        public decimal DefactoFee { get; set; }
        public List<int> Artists { get; set; }

        public int Manager { get; set; }
        public int TalentId { get; set; }
    }
}