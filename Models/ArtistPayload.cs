﻿using System.ComponentModel.DataAnnotations;

namespace Uranium.Models
{
    public class ArtistPayload
    {
        [Required]
        public string DateOfBirth { get; set; }
        public int OperatingCurrency { get; set; }
        public string Avatar { get; set; }
        public int AirlinePreference { get; set; }
        public string AirportPreference { get; set; }
        public int Manager { get; set; }
        public bool AllowAccess { get; set; }
    }
}