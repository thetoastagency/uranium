﻿using System.ComponentModel.DataAnnotations;

namespace Uranium.Models
{
    public class UserPayload
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        [Required]
        [MaxLength(255)]
        public string Email { get; set; }

        [Required]
        [MaxLength(50)]
        public string Password { get; set; }
    }
}