﻿namespace Uranium.Models
{
    public class CalendarRequestPayload
    {
        public int[] Talent { get; set; }
        public long StartDate { get; set; }
        public int Days { get; set; }
    }
}