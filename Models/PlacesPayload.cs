﻿namespace Uranium.Models
{
    public class PlacesPayload
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public string Foursquare { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string Description { get; set; }

        public AddressPayload Address { get; set; }
    }
}