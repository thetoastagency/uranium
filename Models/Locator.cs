﻿namespace Uranium.Models
{
    public class Locator
    {
        public int Ref { get; set; }
        public string Locate { get; set; }
        public int Locating { get; set; }
    }
}