﻿using System.ComponentModel.DataAnnotations;

namespace Uranium.Models
{
    public class AuthenticationPayload
    {
        [Required]
        [MaxLength(255)]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}