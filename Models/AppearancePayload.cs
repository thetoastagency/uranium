﻿namespace Uranium.Models
{
    public class AppearancePayload
    {
        public int PlaceId { get; set; }
        public int RiderId { get; set; }
        public int TalentId { get; set; }
        public int AgreementId { get; set; }
        public int PurchaserId { get; set; }
        public int CalendarId { get; set; }
        public int ContractingCompany { get; set; }
        public int OperatingCurrency { get; set; }
        public long SetStart { get; set; }
        public long SetEnd { get; set; }
        public int[] Contacts { get; set; }
        public CashToCollectPayload CashToCollect { get; set; }
        public int FeeModel { get; set; }
        public string Description { get; set; }
        public string ItineraryNotes { get; set; }
        public int AppearanceId { get; set; }
        public bool Cancelled { get; set; }
    }
}