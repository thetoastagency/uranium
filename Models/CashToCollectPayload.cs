﻿namespace Uranium.Models
{
    public class CashToCollectPayload
    {
        public decimal Amount { get; set; }
        public int CurrencyId { get; set; }
    }
}