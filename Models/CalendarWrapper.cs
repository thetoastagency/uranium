﻿using System.Collections.Generic;
using AtomicCore.Model.Calendar;
using AtomicData;

namespace Uranium.Models
{
    public class CalendarWrapper
    {
        public int Day { get; set; }
        public DateBlob Date { get; set; }
        public List<CalendarMeta> Entries { get; set; }
    }
}