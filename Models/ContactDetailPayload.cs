﻿using System.ComponentModel.DataAnnotations;

namespace Uranium.Models
{
    public class ContactDetailPayload
    {
        [MaxLength(255)]
        public string Detail { get; set; }
        public int Type { get; set; }
    }
}