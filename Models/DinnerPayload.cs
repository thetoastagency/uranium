﻿namespace Uranium.Models
{
    public class DinnerPayload
    {
        public int DinnerId { get; set; }
        public int CalendarId { get; set; }
        public int DinerId { get; set; }
        public int? PayeeId { get; set; }
        public int? PlaceId { get; set; }
        public string Description { get; set; }
        public decimal? Cost { get; set; }
        public int AttachedAppearance { get; set; }
        public long Starting { get; set; }
        public long Ending { get; set; }
    }
}