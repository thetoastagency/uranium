﻿namespace Uranium.Models
{
    public class FlightPaxPayload
    {
        public int Passenger { get; set; }
        public string Class { get; set; }
        public string Locator { get; set; }
        public string Mileage { get; set; }
        public string Seat { get; set; }
        public string Meal { get; set; }
        public string Notes { get; set; }
        public string TicketNumber { get; set; }
        public decimal Cost { get; set; }
        public int CurrencyId { get; set; }
    }
}