﻿using System;

namespace Uranium.Models
{
    public class AccountPayload
    {
        public int AccountId { get; set; }
        public int AppearanceId { get; set; }
        public int FeeType { get; set; }

        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Detail { get; set; }
        public decimal Amount { get; set; }
        public int Currency { get; set; }
        public bool TaxApplicable { get; set; }
        public bool TaxInclusive { get; set; }
        public decimal TaxRate { get; set; }

        public int RefId { get; set; }
    }
}