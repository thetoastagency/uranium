﻿namespace Uranium.Models
{
    public class AppearanceContactPayload
    {
        public int AppearanceId { get; set; }
        public int ContactId { get; set; }
    }
}