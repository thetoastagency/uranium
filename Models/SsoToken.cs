﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Uranium.Models
{
    public class SsoToken
    {
        [JsonProperty]
        public string Sso { get; set; }

        [JsonProperty]
        public string Email { get; set; }

        [JsonProperty]
        public int ClientId { get; set; }

        [JsonProperty]
        public int UserId { get; set; }

        [JsonProperty]
        public int ContactId { get; set; }

        [JsonProperty]
        public bool IsMasterAccount { get; set; }
    }
}