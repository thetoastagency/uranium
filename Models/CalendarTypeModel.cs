﻿namespace Uranium.Models
{
    public class CalendarTypeModel
    {
        public int CalendarTypeId { get; set; }
        public string CalendarTypeLabel { get; set; }
    }
}