﻿namespace Uranium.Models
{
    public class Location
    {
        public int PlaceId { get; set; }
        public string Place { get; set; }
        public long DateTime { get; set; }
    }
}