﻿namespace Uranium.Models
{
    public class AgreementPayload
    {
        public int AgreementId { get; set; }
        public string Agreement { get; set; }
        public string Reference { get; set; }
        public int AppearanceId { get; set; }
        public string Personal { get; set; }
        public string Ground { get; set; }
        public string Transport { get; set; }
        public string Accommodation { get; set; }
        public string TechnicalDiagramUrl { get; set; }
        public string Technical { get; set; }
        public string Misc { get; set; }
        public string Marketing { get; set; }
        public string PaymentTerms { get; set; }
    }
}