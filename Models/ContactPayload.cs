﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Uranium.Models
{
    public class ContactPayload
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        public int Type { get; set; }
        public int ContactId { get; set; }

        public List<ContactDetailPayload> ContactDetail { get; set; }
        public List<AddressPayload> Addresses { get; set; }
        public string Avatar { get; set; }
        public string SearchTags { get; set; }
        public bool AccessEnabled { get; set; }

        public ArtistPayload Artist { get; set; }
    }
}