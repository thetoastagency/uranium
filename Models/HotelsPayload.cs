﻿namespace Uranium.Models
{
    public class HotelsPayload
    {
        public int HotelStayId { get; set; }
        public int CalendarId { get; set; }
        public int PlaceId { get; set; }
        public int PayeeId { get; set; }
        public int CurrencyId { get; set; }
        public int GuestContactId { get; set; }
        public int TalentId { get; set; }
        public decimal Cost { get; set; }
        public string HotelClass { get; set; }
        public string ReservationNumber { get; set; }
        public string StayDescription { get; set; }
        public long CheckinDateTime { get; set; }
        public long CheckoutDateTime { get; set; }
    }
}