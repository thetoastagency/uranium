﻿using System.ComponentModel.DataAnnotations;

namespace Uranium.Models
{
    public class AddressPayload
    {
        public int AddressId { get; set; }

        [Required]
        [MaxLength(255)]
        public string Address1 { get; set; }

        [MaxLength(255)]
        public string Address2 { get; set; }

        [MaxLength(255)]
        public string Address3 { get; set; }

        [MaxLength(255)]
        public string Address4 { get; set; }

        [Required]
        [MaxLength(200)]
        public string City { get; set; }

        [MaxLength(12)]
        public string Postcode { get; set; }
        
        [Required]
        [MaxLength(60)]
        public string Country { get; set; }
        
        public int CityId { get; set; }
        public int CountryId { get; set; }
    }
}