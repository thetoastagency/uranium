﻿namespace Uranium.Models
{
    public class FinancialDetailPayload
    {
        public int Id { get; set; }
        public string Detail { get; set; }
        public int Type { get; set; }
    }
}