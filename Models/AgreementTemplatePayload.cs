﻿namespace Uranium.Models
{
    public class AgreementTemplatePayload
    {
        public string Name { get; set; }
        public string Agreement { get; set; }
    }
}