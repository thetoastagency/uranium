﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Uranium.Models
{
    public class PdfResponse
    {
        public string Path { get; set; }
        public DateTime Generated { get; set; }
    }
}