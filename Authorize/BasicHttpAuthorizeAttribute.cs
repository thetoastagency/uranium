﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Http;

namespace Uranium.Authorize
{
    public class BasicHttpAuthorizeAttribute : AuthorizeAttribute
    {
        bool _requireAuthentication = true;
        bool _requireSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["RequireSsl"]);
        private readonly string _apiuser = ConfigurationManager.AppSettings["ApiUser"];
        private readonly string _apipassword = ConfigurationManager.AppSettings["ApiPassword"];

        public bool RequireSsl
        {
            get { return _requireSsl; }
            set { _requireSsl = value; }
        }
        public bool RequireAuthentication
        {
            get { return _requireAuthentication; }
            set { _requireAuthentication = value; }
        }
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (Authenticate(actionContext) || !RequireAuthentication) { return; }
            HandleUnauthorizedRequest(actionContext);
        }
        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var challengeMessage = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            challengeMessage.Headers.Add("WWW-Authenticate", "Basic");
            challengeMessage.Content = new StringContent("[{\"message\":\"Unauthorised. No payload to authenticate.\"}]");
            throw new HttpResponseException(challengeMessage);
        }
        private bool Authenticate(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (RequireSsl && !HttpContext.Current.Request.IsSecureConnection && !HttpContext.Current.Request.IsLocal)
            {
                return false;
            }

            if (!HttpContext.Current.Request.Headers.AllKeys.Contains("Authorization")) return false;

            string authHeader = HttpContext.Current.Request.Headers["Authorization"];

            IPrincipal principal;
            if (TryGetPrincipal(authHeader, out principal))
            {
                HttpContext.Current.User = principal;
                return true;
            }
            return false;
        }
        private bool TryGetPrincipal(string authHeader, out IPrincipal principal)
        {
            var creds = ParseAuthHeader(authHeader);
            if (creds != null)
            {
                if (TryGetPrincipal(creds[0], creds[1], out principal)) return true;
            }

            principal = null;
            return false;
        }
        private static string[] ParseAuthHeader(string authHeader)
        {
            if (string.IsNullOrEmpty(authHeader) || !authHeader.StartsWith("Basic")) return null;
            var base64Credentials = authHeader.Substring(6);
            var credentials = Encoding.ASCII.GetString(Convert.FromBase64String(base64Credentials)).Split(new char[] { ':' });

            if (credentials.Length != 2 || string.IsNullOrEmpty(credentials[0]) || string.IsNullOrEmpty(credentials[0])) return null;
            return credentials;
        }
        private bool TryGetPrincipal(string username, string password, out IPrincipal principal)
        {
            username = username.Trim();
            password = password.Trim();

            if(username == _apiuser && password == _apipassword)
            {
                principal = null;
                return true;
            }
            else
            {
                principal = null;
                return false;    
            }
        }
    }
}