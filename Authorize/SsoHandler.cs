﻿using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using AtomicCore;

namespace Uranium.Authorize
{
    public class SsoHandler : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string token;
            try
            {
                token = actionContext.Request.Headers.GetValues("SSO-Token").First();
            }
            catch
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("[{\"message\":\"Missing SSO-Token.\"}]")
                };
                return;
            }

            try
            {
                var user = UserObject.GetUser(RSA.Decrypt(token));
                var routeData = actionContext.ControllerContext.RouteData;
                    routeData.Values["user-guid"] = user.Guid;
                    routeData.Values["user-id"] = user.UserId;
                    routeData.Values["client-id"] = user.ClientId;
                base.OnActionExecuting(actionContext);
            }
            catch
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                {
                    Content = new StringContent("[{\"message\":\"Unauthorised client.\"}]")
                };
                return;
            }
        }
    }
}