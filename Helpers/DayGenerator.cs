﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Uranium.Helpers
{
    public class DayGenerator
    {
        public static IEnumerable<DateTime> Generate(DateTime start, DateTime end)
        {
            if (end < start)
                throw new ArgumentException("Cannot generate calendar when end date is less than start date.");

            while (start <= end)
            {
                yield return start;
                start = start.AddDays(1);
            }
        }
    }
}