﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Uranium.Helpers
{
    public class Generics
    {
        public static bool IsNullOrDefault<T>(T value)
        {
            return Equals(value, default(T));
        }
    }
}